#!/usr/bin/env python
import argparse
import binascii


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Filename to Hex")
    parser.add_argument("path", type=str, help="Path to file")
    args = parser.parse_args()

    with open(args.path, "rb") as f:
        content = f.read()
    print(binascii.hexlify(content))
