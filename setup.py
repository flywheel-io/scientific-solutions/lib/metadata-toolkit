# coding: utf-8

"""
    Flywheel Metadata Toolkit
"""
import os
import sys

from setuptools import setup, find_packages
from setuptools.command.install import install

NAME = "flywheel-metadata-toolkit"
# To install the library, run the following
#
# python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

INSTALL_REQUIRES = ["fs~=2.4.0", "pydicom>=2.0.0,<3.0.0"]

DEV_REQUIRES = [
    "black==20.8b1",
    "pytest~=5.1.0",
    "pytest-cov~=2.7.0",
]

VERSION = os.getenv("CI_COMMIT_TAG", "0.0.0.dev1")

setup(
    name=NAME,
    version=VERSION,
    description="Flywheel Metadata Toolkit",
    author_email="support@flywheel.io",
    url="",
    keywords=["Flywheel", "flywheel", "data", "metadata", "toolkit"],
    install_requires=INSTALL_REQUIRES,
    extras_require={"dev": DEV_REQUIRES},
    packages=find_packages(),
    include_package_data=True,
    license="MIT",
    project_urls={"Source": "https://gitlab.com/flywheel-io/public/metadata-toolkit"},
    zip_safe=False,
    long_description="""
Flywheel Metadata Toolkit
==========================

A toolkit for extracting and formatting metadata from files for Flywheel
""",
)
