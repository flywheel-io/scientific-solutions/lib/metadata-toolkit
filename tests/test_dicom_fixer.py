from copy import copy
import filecmp
import os
import tempfile

import pydicom
from pydicom.tag import Tag
from pydicom import config
from pydicom.datadict import dictionary_VR
from pydicom.dataelem import RawDataElement, DataElement_from_raw
from pydicom.data import get_testdata_files
import pytest

from flywheel_metadata.file.dicom.fixer import (
    converter_exception_callback,
    backslash_in_VM1_string_callback,
    fw_pydicom_config,
    fw_data_element_callback,
)
from flywheel_metadata.file.dicom import load_dicom
from flywheel_metadata.file.dicom.tracker import Tracker, RawDataElementTracker


def test_context_manager():
    assert config.data_element_callback == None
    assert config.data_element_callback_kwargs == {}
    with fw_pydicom_config():
        assert config.data_element_callback == fw_data_element_callback
        assert config.data_element_callback_kwargs != {}

    assert config.data_element_callback == None
    assert config.data_element_callback_kwargs == {}
    # test exception handling
    try:
        with fw_pydicom_config():
            assert False
    except AssertionError:
        assert config.data_element_callback == None
        assert config.data_element_callback_kwargs == {}


def test_context_manager_as_decorator():
    assert config.data_element_callback == None
    assert config.data_element_callback_kwargs == {}

    @fw_pydicom_config()
    def config_callbacks():
        assert config.data_element_callback == fw_data_element_callback
        assert config.data_element_callback_kwargs != {}

    config_callbacks()
    assert config.data_element_callback == None
    assert config.data_element_callback_kwargs == {}


def test_backslash_in_VM1_string_callback():
    raw_elem = RawDataElement(
        0x0008103E, "LO", 36, b"Lung 2.5 venous\\Axial.Ref CE  Axial ", 770, False, True
    )
    raw_elem_ret = backslash_in_VM1_string_callback(raw_elem)
    assert raw_elem_ret.value == b"Lung 2.5 venous/Axial.Ref CE  Axial "

    raw_elem_ret = backslash_in_VM1_string_callback(raw_elem, encodings=["latin1"])
    assert raw_elem_ret.value == b"Lung 2.5 venous/Axial.Ref CE  Axial "

    raw_elem_ret = backslash_in_VM1_string_callback(raw_elem, encodings="latin1")
    assert raw_elem_ret.value == b"Lung 2.5 venous/Axial.Ref CE  Axial "


def test_converter_exception_callback_with_invalid_VR_but_in_dictionary():
    raw_elem = RawDataElement(
        Tag(0x0008103E),
        "0x0000",
        26,
        b"decode me when VR is fixed",
        770,
        False,
        True,
    )
    raw_elem_fix = converter_exception_callback(copy(raw_elem))
    assert raw_elem_fix.VR == "LO"

    # Check that DataElement_from_raw infer VR correctly
    with fw_pydicom_config():
        de = DataElement_from_raw(raw_elem)
    assert de.VR == "LO"
    assert de.value == "decode me when VR is fixed"


def test_converter_exception_callback_with_invalid_VR_for_00080005():
    raw_elem = RawDataElement(
        Tag(0x00080005),
        "UN",
        14,
        b"iso8859",
        770,
        False,
        True,
    )
    raw_elem_fix = converter_exception_callback(raw_elem)
    assert raw_elem_fix.VR == "CS"


def test_converter_exception_callback_with_invalid_VR_but_NOT_in_dictionary():
    raw_elem = RawDataElement(
        Tag(0x0009103E),
        "0x0000",
        15,
        b"Do not touch me",
        770,
        False,
        True,
    )
    raw_elem_fix = converter_exception_callback(copy(raw_elem))
    assert raw_elem_fix.VR == "UN"

    # Check that DataElement_from_raw infer VR correctly
    with fw_pydicom_config():
        de = DataElement_from_raw(raw_elem)
    assert de.VR == "UN"
    assert de.value == b"Do not touch me"


def test_converter_exception_callback_on_sequence_delimiter():
    raw_elem = RawDataElement(Tag(0xFFFEE0DD), "0x0000", 0, None, 9150, False, True)
    raw_elem = converter_exception_callback(copy(raw_elem))
    assert raw_elem.VR == "OB"
    with fw_pydicom_config():
        de = DataElement_from_raw(raw_elem)
    assert de.VR == "OB"


def test_load_pydicom_dataset_with_invalid_VR_inside_sequence(dicom_file):
    path = dicom_file("invalid", "invalid_VR_in_UN_sequence.dcm")
    dcm = pydicom.dcmread(path, force=True)
    with pytest.raises(NotImplementedError):
        # this is the faulty tag. 0x00189346 has VR=UN in the original dicom
        # but because pydicom.config.replace_un_with_known_VR=True by default
        # it get decoded as a SQ and pydicom bump into which has an invalid
        # VR=0x0006 which raised an NotImplementedError
        _ = dcm[0x00189346][0][0x00080100]

    dcm = load_dicom(path, force=True)
    de = dcm[0x00189346][0][0x00080100]
    assert de.VR == "SH"
    assert de.value == ""


def test_load_pydicom_dataset_with_invalid_public_tag(dicom_file):
    path = dicom_file("invalid", "explicit_VR_invalid_public_tag.dcm")
    dcm = pydicom.dcmread(path, force=True)
    de = dcm[0x000C0010]  # would raised on pydicom < 2.1.0
    assert de.VR == "UN"

    dcm = load_dicom(path, force=True)
    de = dcm[0x000C0010]
    assert de.VR == "UN"  # correctly replaced by UN
    assert de.value == b"whatever"


def test_fw_pydicom_config_replace_un_vr_false(dicom_file):
    path = dicom_file("invalid", "invalid_VR_in_UN_sequence.dcm")
    with fw_pydicom_config(replace_un_with_known_vr=False):
        assert config.replace_un_with_known_vr is False
        dcm = pydicom.dcmread(path, force=True)
        dcm.decode()  # if you decode outside of the context manager you'll get SQ
    assert dcm[0x00189346].VR == "UN"
    with fw_pydicom_config():
        dcm = pydicom.dcmread(path, force=True)
        dcm.decode()
    assert dcm[0x00189346].VR == "SQ"


def test_fw_pydicom_config_callback():

    callback_tag_list = list()

    def tag_list_callback(raw_element, **kwargs):
        nonlocal callback_tag_list
        callback_tag_list.append(raw_element.tag)
        return raw_element

    vr_count = 0

    def count_vr_elem_callback(raw_element, **kwargs):
        count_vr = kwargs.get("count_vr", "DS")
        if dictionary_VR(raw_element.tag) == count_vr:
            nonlocal vr_count
            vr_count += 1
        return raw_element

    dcm_path = get_testdata_files("MR_small.dcm")[0]
    dcm_element_list = list()

    def recursive_append(ds):
        nonlocal dcm_element_list
        for elem in ds:
            dcm_element_list.append(elem.tag)
            if elem.VR == "SQ":
                recursive_append(elem)

    # Test data_element_callback on top of default use_fw_data_elem_callback
    with fw_pydicom_config(data_element_callback=tag_list_callback):
        dcm = pydicom.dcmread(dcm_path)
        recursive_append(dcm)
        assert all(x in callback_tag_list for x in dcm_element_list)

    # Test data_element_callback when use_fw_data_elem_callback is False
    with fw_pydicom_config(
        data_element_callback=count_vr_elem_callback,
        use_fw_callback=False,
        count_vr="CS",
    ):
        dcm = pydicom.dcmread(dcm_path)
        dcm.decode()
        assert vr_count == 10
    with fw_pydicom_config(
        data_element_callback=count_vr_elem_callback, use_fw_callback=False
    ):
        vr_count = 0
        dcm = pydicom.dcmread(dcm_path)
        dcm.decode()
        assert vr_count == 14


@pytest.mark.parametrize(
    "path",
    [
        f
        for f in get_testdata_files()
        if f.endswith(".dcm") and not f.endswith("no_meta.dcm")
    ],
)
def test_fw_pydicom_config_on_pydicom_test_files(path):
    with fw_pydicom_config(replace_un_vr=True):
        dcm = pydicom.dcmread(path, force=True)
        dcm.decode()  # if you decode outside of the context manager you'll get SQ


@pytest.mark.parametrize(
    "path",
    [
        f
        for f in get_testdata_files()
        # Skip files have some bytes modified that do not impact tag values outside
        # of SpecificCharacterSet and that do not raise within default pydicom context
        if f.endswith(".dcm")
        and not any(
            bool(x in f)
            for x in [
                "color-p",
                "693",
                "emri_small_jpeg_2k_lossless_too_short.dcm",
                "ExplVR_BigEnd.dcm",
                "rtdose_rle_1frame.dcm",
                "OT-PAL-8-face.dcm",
                "explicit_VR-UN.dcm",
                "no_meta.dcm",
                "truncated",
                "image_dfl.dcm",
                "no_meta_group_length.dcm",
                "rtdose_rle.dcm",
            ]
        )
    ],
)
def test_load_dicom_without_decoding_dataset_does_not_modify_pydicom_testfile(path):
    dcm = load_dicom(path, decode=False, force=True)
    fd, save_path = tempfile.mkstemp()
    os.close(fd)
    with fw_pydicom_config():
        dcm.save_as(save_path, write_like_original=True)
    try:
        assert filecmp.cmp(path, save_path) is True
    finally:
        os.remove(save_path)


@pytest.mark.parametrize(
    "filename",
    [
        "invalid_VR_in_UN_sequence_UN_PatientID.dcm",
        "invalid_VR_in_UN_sequence.dcm",
        "invalid_VR_in_UN_sequence_UN_PatientID.dcm",
    ],
)
def test_load_dicom_without_decoding_dataset_does_not_modify_local_testfile(
    dicom_file, filename
):
    path = dicom_file("invalid", filename)
    dcm = load_dicom(path, decode=False, force=True)
    fd, save_path = tempfile.mkstemp()
    os.close(fd)
    with fw_pydicom_config():
        dcm.save_as(save_path, write_like_original=True)
    try:
        assert filecmp.cmp(path, save_path) is True
    finally:
        os.remove(save_path)


def test_load_dicom_without_decoding_dataset_sets_UN_VRs(dicom_file):
    path = dicom_file("invalid", "invalid_VR_in_UN_sequence_UN_PatientID.dcm")
    # Test context behavior does not replace UN VR
    dcm = load_dicom(path, decode=False, force=True)
    with fw_pydicom_config(replace_un_with_known_vr=False):
        assert config.replace_un_with_known_vr is False
        assert dcm["PatientID"].VR == "UN"
    # Test out of context behavior does replace UN VR
    dcm = load_dicom(path, decode=False, force=True)
    assert dcm["PatientID"].VR == dictionary_VR(dcm["PatientID"].tag)
    assert config.replace_un_with_known_vr is True


def test_load_pydicom_dataset_sets_UN_VRs(dicom_file):
    path = dicom_file("invalid", "invalid_VR_in_UN_sequence_UN_PatientID.dcm")
    # Test context behavior does not replace UN VR
    dcm = pydicom.dcmread(path, force=True)
    with pytest.raises(NotImplementedError):
        dcm.decode()
    dcm = load_dicom(path, config={"replace_un_with_known_vr": False}, force=True)
    assert dcm["PatientID"].VR == "UN"
    # Test out of context behavior does replace UN VR for tags that don't raise
    dcm = load_dicom(path, force=True)
    assert dcm["PatientID"].VR == dictionary_VR(dcm["PatientID"].tag)
    assert dcm["CTDIPhantomTypeCodeSequence"].VR == "SQ"
    for ds in dcm["CTDIPhantomTypeCodeSequence"]:
        assert ds[("3131", "3633")]
    assert config.replace_un_with_known_vr is True


def test_RawDataElementTracker_methods_work_as_expected():
    args = (Tag(0x50F10010), None, 8, b"FDMS 1.0", 0, True, True)
    rdet = RawDataElementTracker(*args)
    assert rdet._original == RawDataElement(*args)
    rdet = rdet._replace(VR="LO")
    assert rdet.VR == "LO"
    assert rdet._events == ["Replace VR: None -> LO"]
    rdet.set_final(*args)
    assert rdet._final == RawDataElement(*args)


def test_Tracker_methods_work_as_expected():
    tracker = Tracker()
    args = (Tag(0x50F10010), None, 8, b"FDMS 1.0", 0, True, True)
    rdet = tracker.track(RawDataElement(*args))
    assert type(rdet) == RawDataElementTracker
    rdet = rdet._replace(VR="LO")
    rdet = rdet._replace(VR=None)
    rde = tracker.release(rdet)
    assert rde == RawDataElement(*args)
    assert tracker.rde_traces[0]["events"][0] == "Replace VR: None -> LO"
    assert tracker.rde_traces[0]["events"][1] == "Replace VR: LO -> None"
    rdet = tracker.track(RawDataElement(*args))
    _ = tracker.release(rdet)  # not doing anything on that RawDataElement
    assert len(tracker.rde_traces) == 2
    tracker.clean()
    assert len(tracker.rde_traces) == 1
    some_good_looking_string = str(tracker)
    assert some_good_looking_string


def test_load_pydicom_track_callbacks_changes(dicom_file):
    path = dicom_file("invalid", "patient_id_invalid_vr.dcm")
    tracker = Tracker()
    _ = load_dicom(path, tracker=tracker, force=True)
    tracker.clean()
    assert len(tracker.rde_traces) == 1
    assert tracker.rde_traces[0]["events"] == ["Replace VR: None -> LO"]


# TODO: add test for dicom that has invalid VR and decoding with inferred value raised
# which should set the dicom VR to OB.
