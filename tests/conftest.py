import os
import pytest
import shutil
import tempfile
from pathlib import Path

ASSETS_ROOT = Path(__file__).parent / "assets"
DICOM_ROOT = ASSETS_ROOT / "DICOM"


@pytest.fixture(scope="function")
def dicom_file():
    def get_dicom_file(folder, filename):
        fd, path = tempfile.mkstemp(suffix=".dcm")
        os.close(fd)

        src_path = os.path.join(DICOM_ROOT, folder, filename)
        shutil.copy(src_path, path)

        return path

    return get_dicom_file
